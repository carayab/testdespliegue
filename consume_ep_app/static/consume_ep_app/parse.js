//Autor Claudio Araya
function consulta_api() {

//Funcion se encarga de obtener los datos de la api, convertirlos en JSON y generar los elemento html para visualizarlos.

    var div_seguro = document.getElementById("seguro");
    div_seguro.innerHTML = '';

    var tabla = document.createElement("table");
    var tblBody = document.createElement("tbody");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var seguro = JSON.parse(this.responseText);

            //Primera Columna con Nombre del Seguro
            var fila = document.createElement("tr");
            var celda = document.createElement("td");
            var txtCel = document.createTextNode(seguro.insurance.name);
            celda.appendChild(txtCel);
            fila.appendChild(celda);
            
            //Segunda Columna con Descripcion del Seguro
            var celda2 = document.createElement("td");
            var parrafo = document.createElement("P");
            var txt = document.createTextNode(seguro.insurance.description+" A un precio de " +seguro.insurance.price);
            parrafo.appendChild(txt);
            var imagen = document.createElement("IMG");
            imagen.setAttribute("src", seguro.insurance.image);
            imagen.setAttribute("width", "250");
            imagen.setAttribute("height", "180");

            celda2.appendChild(imagen);
            celda2.appendChild(parrafo);
            fila.appendChild(celda2);

            tblBody.appendChild(fila);
            tabla.appendChild(tblBody);
            div_seguro.appendChild(tabla);
        };

    };
    xmlhttp.open("GET", "consulta", true);
    xmlhttp.send();
}