from django.test import TestCase

# Create your tests here.
import unittest
from django.test import Client

class SimpleTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_details(self):
        # Solicitar un request GET.
        response = self.client.get('/ConsumeEndPoint/consulta')

        # Validar que la respuesta sea 200 OK.
        self.assertEqual(response.status_code, 200)
