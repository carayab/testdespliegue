from django.apps import AppConfig


class ConsumeEpAppConfig(AppConfig):
    name = 'consume_ep_app'
