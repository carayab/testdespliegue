from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import JsonResponse
import requests

class IndexView(TemplateView):
    template_name = "consume_ep_app/index.html"


def consulta_api(request):
    REINTENTOS = 3
    url = "https://dn8mlk7hdujby.cloudfront.net/interview/insurance/58"
    if request.method == "GET":
        INTENTOS = 0
        while INTENTOS < REINTENTOS:            
            r = requests.get(url)
            if r.status_code == 200:
                return JsonResponse(r.json())
            else:
                INTENTOS += 1
                time.sleep(5)
        return JsonResponse({'error': 'Request failed'})
    else:
        return JsonResponse({'error': 'Method not allowed'})