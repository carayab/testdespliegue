# Test de Ingeniero DevOps

## Sobre la aplicacion ⚙️
_La aplicación está desarrollada en django(python) y realiza un request sobre el endpoint proporcionado, el resultado de este request es un json el cual es utilizado por una función de JavaScript encargada de construir el dom del documento con dicha información._

_Para probar la funcionalidad de la aplicación se debe acceder a la siguiente [URL](http://testdevops-bicevida.devops4cloud.com/ConsumeEndPoint/) y hacer click en el boton "Revisar Seguro"_

## Sobre CI-CD 📦
_Se implementó un pipeline *(.gitlab-ci.yml)* en ambiente GitLab, el cual esta compuesto por 2 stages_

* **Prueba_Unitaria** - Ejecuta una test unitario encargado de validar si el codigo de retorno de la [consulta](http://testdevops-bicevida.devops4cloud.com/ConsumeEndPoint/consulta) es 200.

* **Despliegue** - Por medio de un script *(.config_aws.sh)* primero se instala el cliente de Elastic Beanstalk(eb), posteriormente se configura el entorno para acceder a aws, mediante variables de entorno configuradas en GitLab-CICD, Finalmente se realizan las tareas de despliegue de la aplicacion. Selección de environment de EB, despliegue, y posterior mustra de estado del servicio.


## Sobre La infrestructura como código 🛠️
_Se generó un script de Terraform *(test_iac.tf)* para implementar el aplication y el environment de Elastic Beanstalk_ 


## Sobre el flujo de Git. 🚀
_Para esta prueba no se implementó gitflow ya que la funcionalidad a desarrolalr era solo una, el pipeline se ejecuta con cualquier commit que se haga sobre master._
