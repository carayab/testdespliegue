resource "aws_elastic_beanstalk_application" "testdevops" {
  name        = "testdevops"
  description = "tf-test-desc"
}

resource "aws_elastic_beanstalk_environment" "testdevops-env" {
  name                = "testdevops-env"
  application         = aws_elastic_beanstalk_application.testdevops.name
  solution_stack_name = "64bit Amazon Linux 2 v3.1.3 running Python 3.7"

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "vpc-99b0f1e1"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "subnet-77319c2a"
  }
  
  setting {
      namespace = "aws:autoscaling:launchconfiguration"
      name = "IamInstanceProfile"
      value = "aws-elasticbeanstalk-ec2-role"
  }

}